﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GD_manager
{
    public partial class WaitForm : Form
    {
        public WaitForm()
        {
            InitializeComponent();
        }
        private int num = 0;

        private void WaitForm_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;            
        }

        public void SetStupData()
        {
            this.textBox_stepData.Text = BaseClass.BaseMethod.static_setupData;
            if (BaseClass.BaseMethod.static_setupTitle != "")
            {
                this.Text = BaseClass.BaseMethod.static_setupTitle;
            }
            else
            {
                this.Text = "默认加载";
            }
        }


        private void Timer1_Tick(object sender, EventArgs e)
        {
            num++;
            textBox1.Text = Convert.ToDouble(num * 0.1).ToString("0.0") + "秒";
            SetStupData();
        }

        private void F_wait_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Enabled = false;
        }

    }
}
