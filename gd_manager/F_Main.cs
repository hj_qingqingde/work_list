﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace GD_manager
{
    public partial class F_Main : Form
    {
        public F_Main()
        {
            InitializeComponent();
            this.IsMdiContainer = true;                 
            GetMyFace();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //Console.WriteLine(e.ExceptionObject.ToString());
            MessageBox.Show("错误原因"+System.Environment.NewLine+e.ExceptionObject.ToString());
        }
        
        BaseClass.BaseMethod myBasic = new BaseClass.BaseMethod();
        BaseClass.ComputerData jsj = new BaseClass.ComputerData();
        BaseClass.UserClass udao = new BaseClass.UserClass();

        TreeNode parentNode_kf ;
        TreeNode parentNode_zg ;
        TreeNode parentNode_ck ;
        TreeNode parentNode_cw ;
        TreeNode parentNode_tb ;
        TreeNode parentNode_data ;
        TreeNode parentNode_sys ;
        private int num = 0;
        private string sys_bb = "";

        private void F_Main_Load(object sender, EventArgs e)
        {
            try
            {
                Thread myTh;
                //WaitForm ww = new WaitForm();
                myTh = new Thread(new ThreadStart(myBasic.Waiting));
                
                F_Login login = new F_Login
                {
                    frm = this
                };
                myTh.Start();

                BaseClass.BaseMethod.static_setupTitle = "开始初始化一些常用数据,请等待......";
                SetStartData();
                
                this.state_username.Text = "用户：[" + BaseClass.BaseMethod.static_userno + "]" + BaseClass.BaseMethod.static_username + " " + BaseClass.BaseMethod.static_user_rname + "您好!";
                this.state_ip.Text = "IP：" + jsj.IpAddress;
                this.NowTime.Text = "当前时间: " + DateTime.Now.ToLongDateString() + DateTime.Now.ToLongTimeString();
                BaseClass.INIMethod inifile = new BaseClass.INIMethod();
                if (inifile.GetINIFileValue("Connecttion", "service") == ".")
                {
                    this.state_fwq_IP.Text = "当前服务器地址：127.0.0.1";
                }
                else
                {
                    this.state_fwq_IP.Text = "当前服务器地址：" + inifile.GetINIFileValue("Connecttion", "service");
                }
                timer1.Enabled = true;

                #region
                //BaseClass.BaseMethod.static_setupData = "获取待登陆人权限......";
                //udao.GetpowerData(); //获取权限信息

                //开始构建菜单树            
                InitPersonMenu();
                #endregion
                //sys_bb = "1.0.1.89";                        
                this.state_bb.Text = "当前版本号:" + sys_bb;
                
                //SetMyStyle();
                myTh.Abort();
            }
            catch (Exception ex) {
                myBasic.ShowMessage("Operation",ex.Message);
            }
        }

        //private void SetMyStyle() {
        /*
        foreach (Control con in this.groupBox1.Controls)
        {
            if (con is TextBox)
            {
                con.Text = "";
            }
        }
        */
        //this.BackColor = Color.FromArgb(167,246,246);
        //this.treeView1.BackColor = Color.FromArgb(246,247,221);
        //}
        #region 标准颜色设置 按钮、TextBox、背景、TreeView、工具栏
        private void SetMyStyle()
        {
            this.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.BackGroudColor);
            foreach (Control con in this.Controls)
            {                
                if (con is TextBox)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TextBoxColor);
                }
                else if (con is ToolStrip)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ToolStripColor);
                }
                else if (con is TreeView)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TreeViewColor);
                }
                else if (con is Button)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ButtonColor);
                }
                else if (con is ComboBox)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ComboBoxColor);
                }
                else if (con.HasChildren) {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ButtonColor);
                    foreach (Control conns in con.Controls)
                    {
                        if (conns is TextBox)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TextBoxColor);
                        }
                        else if (conns is ToolStrip)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ToolStripColor);
                        }
                        else if (conns is TreeView)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TreeViewColor);
                        }
                        else if (conns is Button)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ButtonColor);
                        }
                        else if (conns is ComboBox) {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ComboBoxColor);
                        }
                    }
                }                
            }
        }
        #endregion


        #region 构建个人菜单
        private void InitPersonMenu() {
            BaseClass.BaseMethod.static_setupData = "初始化个人菜单......";
            //if (BaseClass.BaseMethod.static_menu.Count == 0) { MessageBox.Show("菜单初始化失败！请点击 我的菜单 重新加载菜单！"); }  //如果菜单开始没有加载全则重新加载
            
            //建立顶级节点
            TreeNode topNode = new TreeNode();
            topNode = treeView1.Nodes.Add("我的菜单");
            //二级节点和基础节点全部动态加载
            string temp_menu_no="";
            #region 菜单初始化
            int kf = 0, zg = 0, ck = 0, cw = 0, tb = 0, data = 0, sys = 0;
            for (int i = 0; i < BaseClass.BaseMethod.static_power.Count; i++) {
                temp_menu_no="";
                if (BaseClass.BaseMethod.static_power[i].Menu_no != "") {
                    var a = from b in BaseClass.BaseMethod.static_menu where b.M_no == BaseClass.BaseMethod.static_power[i].Menu_no select b;
                    if (a.ToList<BaseClass.BaseMethod.Base_menu>().Count > 0)
                    {
                        if (a.ToList<BaseClass.BaseMethod.Base_menu>()[0].M_level == "2")
                        {
                            temp_menu_no = BaseClass.BaseMethod.static_power[i].Menu_no.Substring(0, 4);
                            //建立基础节点
                            //TreeNode parentNode_kf;
                            //TreeNode parentNode_zg;
                            //TreeNode parentNode_ck;
                            //TreeNode parentNode_cw;
                            //TreeNode parentNode_tb;
                            //TreeNode parentNode_data;
                            //TreeNode parentNode_sys;

                            switch (temp_menu_no)
                            {
                                case "kf_m":
                                    if (kf == 0)
                                    {
                                        parentNode_kf = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_kf);
                                        kf = 1;
                                    }
                                    parentNode_kf.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "zg_m":
                                    if (zg == 0)
                                    {
                                        parentNode_zg = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_zg);
                                        zg = 1;
                                    }
                                    parentNode_zg.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "ck_m":
                                    if (ck == 0)
                                    {
                                        parentNode_ck = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_ck);
                                        ck = 1;
                                    }
                                    parentNode_ck.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "cw_m":
                                    if (cw == 0)
                                    {
                                        parentNode_cw = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_cw);
                                        cw = 1;
                                    }
                                    parentNode_cw.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "tb_m":
                                    if (tb == 0)
                                    {
                                        parentNode_tb = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_tb);
                                        tb = 1;
                                    }
                                    parentNode_tb.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "base":
                                    if (data == 0)
                                    {
                                        parentNode_data = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_data);
                                        data = 1;
                                    }
                                    parentNode_data.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                case "sys_":
                                    if (sys == 0)
                                    {
                                        parentNode_sys = new TreeNode(GetTopMenuName(BaseClass.BaseMethod.static_power[i].Menu_no));
                                        topNode.Nodes.Add(parentNode_sys);
                                        sys = 1;
                                    }
                                    parentNode_sys.Nodes.Add(BaseClass.BaseMethod.static_power[i].Menu_name);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            #endregion

            treeView1.ExpandAll();
            treeView1.CheckBoxes = false;
            Font font = new Font("UTF-8", myBasic.GetSizeByType("menu_font_size"));//UTF-8是字体的编码格式，2是字体大小
            treeView1.Font = font;
            //this.treeView1.BackColor = System.Drawing.SystemColors.MenuHighlight;            
        }
        private string GetTopMenuName(string menu_no) {
            string m_name = string.Empty;
            menu_no = menu_no.Substring(0, 4);  //截取字符串 kf_m zg_m
            var a = from b in BaseClass.BaseMethod.static_menu where b.M_no.Substring(0,4)==menu_no && b.M_level=="1" select b;
            if (a.ToList<BaseClass.BaseMethod.Base_menu>().Count > 0)
            {
                m_name = a.ToList<BaseClass.BaseMethod.Base_menu>()[0].M_name;
            }
            return m_name;
        }
        #endregion    

        #region 皮肤设置
        private void GetMyFace()
        {
            string path_faceName = "";
            BaseClass.INIMethod iniFile = new BaseClass.INIMethod();
            path_faceName = iniFile.GetINIFileValue("MyFace_default", "path_name"); //MyFace_default  path_name
            if (path_faceName != "")
            {
                this.skinEngine1.SkinFile = Application.StartupPath + "\\skins\\" + path_faceName;
            }
            else
            {
                MessageBox.Show("没有读取到皮肤文件！", "提示");
            }
        }
        #endregion

        #region 初始化信息
        private void SetStartData()
        {
            BaseClass.BaseMethod.static_setupData = "获取初始化信息......";            
            IniData_User();
            //MessageBox.Show("用户"+BaseClass.BaseMethod.static_user.Count+" 次数"+num);
            num = 0;
            IniData_Power();
            //MessageBox.Show("权限" + BaseClass.BaseMethod.static_power.Count + " 次数" + num);
            num = 0;
            IniData_Menu();
            //MessageBox.Show("菜单" + BaseClass.BaseMethod.static_menu.Count + " 次数" + num);
            num = 0;
            IniData_Custom();
            num = 0;
            IniData_level();
            SetIniData();   //这部分内容不需要100%加载成功

            #region 版本校验
            FileVersionInfo myFileVersion = FileVersionInfo.GetVersionInfo(System.Windows.Forms.Application.ExecutablePath);
            sys_bb = myFileVersion.FileVersion;
            if (!udao.IsOK_system_now(sys_bb))
            {
                //如果不一致,则判断是否有最新系统程序,如果有新程序,则提示是否升级
                if (udao.IsNew_system_file())
                {
                    if (MessageBox.Show("发现系统更新文件,是否立刻打开系统升级程序进行系统更新！", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        //启动升级程序
                        string update_file_name = "";
                        BaseClass.INIMethod ini_file = new BaseClass.INIMethod();
                        update_file_name = ini_file.GetINIFileValue("Update_File", "name");
                        if (System.IO.File.Exists(Application.StartupPath + "\\" + update_file_name) == false)
                        {
                            MessageBox.Show("路径" + Application.StartupPath + "\\" + update_file_name + " 不存在！缺少升级程序！", "提示");
                            this.Close();
                        }
                        Process pr = new Process();//声明一个进程类对象
                        pr.StartInfo.FileName = Application.StartupPath + "\\" + update_file_name;
                        pr.Start();

                        udao.UpdateExistFlag();
                        System.Environment.Exit(0);
                    }
                    else
                    {
                        System.Environment.Exit(0);
                    }
                }
                else
                {
                    MessageBox.Show("当前程序版本是" + sys_bb + "和数据库中记录版本不一致,请联系管理员更新系统版本", "提示");
                    System.Environment.Exit(0);
                }
            }

            #endregion

        }

        private void IniData_User() {
            if (num == 0)
            {
                BaseClass.BaseMethod.static_setupData = "加载人员信息......";
                udao.ResetBasicUser();        //加载人员信息   
                var a = from b in BaseClass.BaseMethod.static_user where b.U_no == BaseClass.BaseMethod.static_userno select b;
                if (a.ToList<BaseClass.BaseMethod.Base_user>().Count > 0)
                {
                    BaseClass.BaseMethod.static_username = a.ToList<BaseClass.BaseMethod.Base_user>()[0].U_name;
                    BaseClass.BaseMethod.static_user_rname = a.ToList<BaseClass.BaseMethod.Base_user>()[0].R_name;
                }
            }
            else
            {
                if (BaseClass.BaseMethod.static_user.Count == 0)
                {
                    num++;
                    BaseClass.BaseMethod.static_setupData = "第" + num + "次尝试" + "休息一秒,重新获取待用户信息......";
                    udao.ResetBasicUser();        //加载人员信息
                    var a = from b in BaseClass.BaseMethod.static_user where b.U_no == BaseClass.BaseMethod.static_userno select b;
                    if (a.ToList<BaseClass.BaseMethod.Base_user>().Count > 0)
                    {
                        BaseClass.BaseMethod.static_username = a.ToList<BaseClass.BaseMethod.Base_user>()[0].U_name;
                        BaseClass.BaseMethod.static_user_rname = a.ToList<BaseClass.BaseMethod.Base_user>()[0].R_name;
                    }
                    IniData_User();
                }
                else {
                    num = 0;
                }
            }
        }
        private int power_nm = 0;
        private void IniData_Power()
        {
            power_nm = 0;

            while (power_nm < 10)
            {
                power_nm++;
                BaseClass.BaseMethod.static_setupData = "第" + power_nm + "次" + "获取待登陆人权限......";
                udao.GetpowerData();                

                if (BaseClass.BaseMethod.static_power.Count > 0)
                {
                    break;
                }
                else {
                    Thread.Sleep(1000);
                }
            }
        }

        private void IniData_Menu()
        {
            num++;
            BaseClass.BaseMethod.static_setupData = "第" + num + "次获取待基本菜单信息......";
            if (BaseClass.BaseMethod.static_menu.Count == 0)
            {

                udao.ResetBesicMenu("1");
                IniData_Menu();
            }
            else
            {
                num = 0;
            }            
        }

        private void IniData_Custom()
        {
            num++;
            BaseClass.BaseMethod.static_setupData = "第" + num + "次获取待客户信息......";
            if (num<10 && BaseClass.BaseMethod.static_customer.Count == 0)
            {

                udao.ResetBasicCustom("1");
                IniData_Custom();
            }
            else
            {
                num = 0;
            }
        }
        private void IniData_level()
        {
            num++;
            BaseClass.BaseMethod.static_setupData = "第" + num + "次获取待等级信息......";
            if (BaseClass.BaseMethod.static_level.Count == 0)
            {

                udao.ResetBasicLevel("1");
                IniData_level();
            }
            else
            {
                num = 0;
            }
        }

        private void IniData_Role()
        {
            num++;
            BaseClass.BaseMethod.static_setupData = "第" + num + "次获取待角色信息......";
            if (BaseClass.BaseMethod.static_role.Count == 0)
            {

                udao.ResetBasicRole("1");
                IniData_Role();
            }
            else
            {
                num = 0;
            }
        }

        #endregion

        #region 加载初始化基本信息
        private void SetIniData()
        {
            BaseClass.BaseMethod.static_setupData = "加载角色信息......";
            udao.ResetBasicRole("1");              //加载角色信息
            //BaseClass.BaseMethod.static_setupData = "加载菜单信息......";
            //udao.ResetBesicMenu("1");              //加载菜单信息
            //BaseClass.BaseMethod.static_setupData = "加载人员信息......";
            //udao.ResetBasicUser();        //加载人员信息
            //BaseClass.BaseMethod.static_setupData = "加载客户信息......";
            //udao.ResetBasicCustom("1");     //加载客户信息
            BaseClass.BaseMethod.static_setupData = "加载账户信息......";
            udao.ResetBasicAccount("1");         //加载账户信息           
            BaseClass.BaseMethod.static_setupData = "加载部门信息......";
            udao.ResetBasicDepartment("1");         //加载部门信息            
            BaseClass.BaseMethod.static_setupData = "加载等级信息......";
            udao.ResetBasicLevel("1");         //加载部门信息            
            BaseClass.BaseMethod.static_setupData = "加载附加费信息......";
            udao.ResetBasicAddChange("1");         //加载部门信息           
        }
        #endregion

        /// <summary>
        /// 响应点击菜单,并打开对应窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //MessageBox.Show(e.Node.Text);
            string menu_name = string.Empty;
            menu_name = e.Node.Text;
            if (menu_name == "我的菜单") {
                #region 清空历史菜单信息
                
                if (MessageBox.Show("是否刷新菜单！", "提示", MessageBoxButtons.YesNo)==DialogResult.Yes) {
                    Thread myTh;
                    //WaitForm ww = new WaitForm();
                    myTh = new Thread(new ThreadStart(myBasic.Waiting));
                    myTh.Start();
                    BaseClass.BaseMethod.static_setupData = "重新构建菜单";
                    try
                    {
                        if (treeView1.Nodes.Count >= 0)
                        {
                            foreach (TreeNode sNode in treeView1.Nodes)
                            {
                                if (sNode != null)
                                {
                                    //MessageBox.Show(sNode.Text);
                                    foreach (TreeNode tNode in sNode.Nodes)
                                    {
                                        if (tNode != null)
                                        {
                                            foreach (TreeNode tn in tNode.Nodes)
                                            {
                                                if (tn != null)
                                                {
                                                    //MessageBox.Show("二级" + tNode.Text + System.Environment.NewLine + "三级" + tn.Text);
                                                    tn.Nodes.Clear();
                                                    //tNode.Nodes.Remove(tn); //清空二级菜单 等效与 treeView1.Nodes.Remove(tn);
                                                }
                                            }
                                            tNode.Nodes.Clear();
                                        }
                                    }
                                }
                            }
                            treeView1.Nodes.Clear();
                        }
                        udao.GetpowerData();    //强制重新加载个人权限菜单
                        SetStartData();
                        InitPersonMenu();
                    }catch(Exception ex)
                    {
                        myBasic.ShowMessage("FService",ex.Message+System.Environment.NewLine+BaseClass.BaseMethod.static_error);
                    }
                    myTh.Abort();
                }
                
                #endregion
            }
            else { 
            var a = from b in BaseClass.BaseMethod.static_menu where b.M_name == menu_name && b.M_level=="2" select b;
                if (a.ToList<BaseClass.BaseMethod.Base_menu>().Count > 0)
                {
                    string menu_no = "";
                    menu_no = a.ToList<BaseClass.BaseMethod.Base_menu>()[0].M_no;
                    switch (menu_no)
                    {
                        #region 客服菜单
                        case "kf_m_1":
                            Open_kf_SheetSelect(menu_name);
                            break;
                        case "kf_m_3":
                            Open_kf_AddChangeSelect(menu_name);
                            break;
                        case "kf_m_5":
                            Open_kf_AddChangeSelect_fk(menu_name);
                            break;
                        #endregion
                        #region 主管菜单
                        case "zg_m_1":
                            Open_zg_SheetSelect(menu_name);
                            break;
                        case "zg_m_3":
                            Open_zg_Deblocking(menu_name);
                            break;
                        #endregion
                        #region 仓库菜单
                        case "ck_m_1":
                            Open_ck_SheetSelect(menu_name);
                            break;
                        #endregion
                        #region 财务菜单
                        case "cw_m_1":
                            Open_cw_SaveMoneySheet(menu_name);
                            break;
                        case "cw_m_3":
                            Open_cw_Pass_GDSheet(menu_name);
                            break;
                        case "cw_m_5":
                            Open_cw_Pass_AddChangeSheet(menu_name);
                            break;
                        #endregion
                        #region 报表菜单
                        case "tb_m_1":
                            //MessageBox.Show("工单历史查询开发中");
                            Open_tb_SheetListIsPass(menu_name);
                            break;
                        case "tb_m_2":
                            //MessageBox.Show("客户扣款流水查询开发中");
                            Open_tb_Customer_ListMoneyFlow(menu_name);
                            break;
                        #endregion
                        #region 档案菜单
                        case "base_m_1":
                            Open_base_UserManager(menu_name);
                            break;
                        case "base_m_2":
                            Open_base_DepartmentManager(menu_name);
                            break;
                        case "base_m_3":
                            Open_base_RoleManager(menu_name);
                            break;
                        case "base_m_4":
                            Open_base_CustomerManager(menu_name);
                            break;
                        case "base_m_5":
                            Open_base_AccountManager(menu_name);
                            break;
                        case "base_m_6":                            
                            Open_base_AddChangeManager(menu_name);
                            break;
                        case "base_m_7":
                            Open_base_LevelManager(menu_name);
                            break;
                        case "base_m_8":
                            Open_base_MenuNameManager(menu_name);
                            break;
                        #endregion
                        #region 系统信息设置
                        case "sys_m_1":
                            Open_sys_UpdatePWD(menu_name);
                            break;
                        case "sys_m_2":
                            Open_sys_FileUpdate(menu_name);
                            break;
                        case "sys_m_3":
                            Open_sys_Look(menu_name);
                            break;
                        case "sys_m_4":
                            Open_sys_PowerForm(menu_name);
                            break;
                        #endregion
                        default:
                            break;
                    }
                }
                else {
                    //myBasic.ShowMessage("FError","点击菜单名称"+menu_name+"，没有查询到！");
                }
            }
        }

        private void Open_kf_AddChangeSelect_fk(string menu_name)
        {
            Mode_kf.F_kf_SheetSelect_kf SelectForm = new Mode_kf.F_kf_SheetSelect_kf(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_tb_Customer_ListMoneyFlow(string menu_name)
        {
            Mode_table.F_tb_Customer_ListMoneyFlow SelectForm = new Mode_table.F_tb_Customer_ListMoneyFlow(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_tb_SheetListIsPass(string menu_name)
        {
            Mode_table.F_tb_SheetListIsPass SelectForm = new Mode_table.F_tb_SheetListIsPass(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_sys_Look(string menu_name)
        {
            Mode_SysForm.F_sys_Look SelectForm = new Mode_SysForm.F_sys_Look(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_sys_FileUpdate(string menu_name)
        {
            Mode_SysForm.F_sys_FileUpdate SelectForm = new Mode_SysForm.F_sys_FileUpdate(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_sys_UpdatePWD(string menu_name)
        {
            Mode_SysForm.F_sys_UpdatePWD SelectForm = new Mode_SysForm.F_sys_UpdatePWD(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_MenuNameManager(string menu_name)
        {
            Mode_BaseData.F_base_MenuManager SelectForm = new Mode_BaseData.F_base_MenuManager(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_LevelManager(string menu_name)
        {
            Mode_BaseData.F_base_LevelManager SelectForm = new Mode_BaseData.F_base_LevelManager(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_AddChangeManager(string menu_name)
        {
            Mode_BaseData.F_base_AddChangeManager SelectForm = new Mode_BaseData.F_base_AddChangeManager(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_AccountManager(string menu_name)
        {
            Mode_BaseData.F_base_Account SelectForm = new Mode_BaseData.F_base_Account(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_CustomerManager(string menu_name)
        {
            Mode_BaseData.F_base_customer_sel SelectForm = new Mode_BaseData.F_base_customer_sel(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_RoleManager(string menu_name)
        {
            Mode_BaseData.F_base_Role SelectForm = new Mode_BaseData.F_base_Role(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_DepartmentManager(string menu_name)
        {
            Mode_BaseData.F_base_Department SelectForm = new Mode_BaseData.F_base_Department(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_base_UserManager(string menu_name)
        {
            Mode_BaseData.F_base_UserManager SelectForm = new Mode_BaseData.F_base_UserManager(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_cw_Pass_AddChangeSheet(string menu_name)
        {
            Mode_cw.F_cw_Pass_AddChangeSheet SelectForm = new Mode_cw.F_cw_Pass_AddChangeSheet(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_cw_Pass_GDSheet(string menu_name)
        {
            Mode_cw.F_cw_Pass_GDSheet SelectForm = new Mode_cw.F_cw_Pass_GDSheet(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_cw_SaveMoneySheet(string menu_name)
        {
            Mode_cw.F_cw_SaveMoneySheet SelectForm = new Mode_cw.F_cw_SaveMoneySheet(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_ck_SheetSelect(string menu_name)
        {
            Mode_ck.F_ck_SheetSelect SelectForm = new Mode_ck.F_ck_SheetSelect(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_zg_Deblocking(string menu_name)
        {
            Mode_zg.F_zg_Deblocking SelectForm = new Mode_zg.F_zg_Deblocking(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_zg_SheetSelect(string menu_name)
        {
            Mode_zg.F_zg_SheetSelect SelectForm = new Mode_zg.F_zg_SheetSelect(menu_name);
            CloseIsExistForm(menu_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            SelectForm.TopLevel = false;               //设置窗口为非顶级窗口
            SelectForm.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            SelectForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            SelectForm.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = menu_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            SelectForm.Show();
        }

        private void Open_kf_AddChangeSelect(string F_name)
        {
            Mode_kf.F_kf_AddChangeSelect F_kf_SheetSelect = new Mode_kf.F_kf_AddChangeSelect(F_name);
            CloseIsExistForm(F_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            F_kf_SheetSelect.TopLevel = false;               //设置窗口为非顶级窗口
            F_kf_SheetSelect.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            F_kf_SheetSelect.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            F_kf_SheetSelect.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = F_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            F_kf_SheetSelect.Show();
        }

        #region 客服查询工单窗口 
        /// <summary>
        /// 窗口名称、菜单名称
        /// </summary>
        /// <param name="F_name">窗口名称</param>
        private void Open_kf_SheetSelect(string F_name)
        {
            Mode_kf.F_kf_SheetSelect F_kf_SheetSelect = new Mode_kf.F_kf_SheetSelect(F_name);
            CloseIsExistForm(F_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            F_kf_SheetSelect.TopLevel = false;               //设置窗口为非顶级窗口
            F_kf_SheetSelect.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            F_kf_SheetSelect.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            F_kf_SheetSelect.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = F_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            F_kf_SheetSelect.Show();
        }
        #endregion

        #region 权限窗口 
        /// <summary>
        /// 窗口名称、菜单名称
        /// </summary>
        /// <param name="F_name">窗口名称</param>
        private void Open_sys_PowerForm(string F_name) {            
            Mode_SysForm.F_sys_PowerManager f_Sys_PowerManager = new Mode_SysForm.F_sys_PowerManager(F_name);            
            CloseIsExistForm(F_name);
            TabPage MyTabPage = new TabPage();      //创建TabPage对象
            f_Sys_PowerManager.TopLevel = false;               //设置窗口为非顶级窗口
            f_Sys_PowerManager.Parent = MyTabPage;             //绑定窗口的父窗口为当前新建TabPage标签
            SetMyTabPageSelect(MyTabPage.ImageIndex);   //设置新建标签为当前选择
            f_Sys_PowerManager.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            f_Sys_PowerManager.Dock = DockStyle.Fill;  //Fill    填充模式            
            MyTabPage.Text = F_name;
            tabControl1.TabPages.Add(MyTabPage);    //增加TabPage到多标签控件            
            f_Sys_PowerManager.Show();
        }
        #endregion

        #region tabcontrol相同窗口只能打开一个
        private void CloseIsExistForm(string name)
        {
            foreach (System.Windows.Forms.TabPage page in this.tabControl1.TabPages)
            {
                //MessageBox.Show(page.Name+" === "+page.Text);
                if (page.Text == name)
                {
                    tabControl1.TabPages.Remove(page);
                }
            }
        }
        #endregion
        #region 点击菜单设置选择标签
        private void SetMyTabPageSelect(int num)
        {
            tabControl1.SelectedIndex = num;
        }
        #endregion

        private void TabControl1_DoubleClick(object sender, EventArgs e)
        {
            int num = 0;
            num = tabControl1.SelectedIndex;
            if (num > 0)
            {
                tabControl1.TabPages.RemoveAt(num);                
            }
        }

        public void CloseWinForm(string winName) {
            int num = 0;
            foreach (System.Windows.Forms.TabPage page in this.tabControl1.TabPages)
            {
                //MessageBox.Show(page.Name+" === "+page.Text);
                if (page.Text == winName)
                {
                    num = page.ImageIndex;
                }
            }
            if (num > 0)
            {
                tabControl1.TabPages.RemoveAt(num);
            }
        }

        private void F_Main_FormClosed(object sender, FormClosedEventArgs e)
        {            
            udao.UpdateExistFlag();
            System.Environment.Exit(0);
        }
    }
}
