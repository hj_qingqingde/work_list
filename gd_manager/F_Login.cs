﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace GD_manager
{
    public partial class F_Login : Form
    {
        public F_Login()
        {
            InitializeComponent();
            BaseClass.BaseMethod.Kill(); //杀死同名进程
        }
        
        
        public F_Main frm;
        //public F_MainSecond frm_Second;
        BaseClass.UserClass userclass = new BaseClass.UserClass();
        F_Main fm = new F_Main();
        BaseClass.INIMethod ini = new BaseClass.INIMethod();

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            string username = this.txtUserName.Text;
            string password = this.txtUserPwd.Text;
            bool strEnter = false;

            if (username.Trim().Length > 20)
            {
                MessageBox.Show("用户名称不能超过10个字节！");
                this.txtUserName.Text = "";
                return;
            }
            if (password.Trim().Length > 12)
            {
                MessageBox.Show("用户密码不能超过12个字节！");
                this.txtUserPwd.Text = "";
                return;
            }

            //Thread myTh;
            //myTh = new Thread(new ThreadStart(proSetUp));
            //myTh.Start();
            this.progressBar1.Visible = true;
            this.progressBar1.Value = 0;
            this.progressBar1.Maximum = 1000;
            this.progressBar1.Step = 10;
            strEnter = userclass.LoginEnter(this.txtUserName.Text, this.txtUserPwd.Text);
            //if (strEnter != "true")
            //{
            //MessageBox.Show("用户名或者密码错误，请重新输入！");
            //}
            //myTh.Abort();
            //MessageBox.Show("结果="+strEnter.ToString());
            if (strEnter)
            {
                ini.WriteTempData("TempData", "user_no", this.txtUserName.Text.Trim());
                ini.WriteTempData("TempData", "user_pwd",BaseClass.ChangeStr.GetNormarAction(this.txtUserPwd.Text.Trim()));

                this.Visible = false;
                BaseClass.BaseMethod.static_userno = this.txtUserName.Text;
                fm.Show();
                //fm.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                //fm.LoadInitData();
                //fm_second.strUserName = txtUserName.Text;
                //fm_second.Show();
                //fm_second.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            
            this.progressBar1.Visible = false;
        }

        private void ProSetUp() {

            this.progressBar1.Visible = true;
            this.progressBar1.Maximum = 1000;
            this.progressBar1.Value = 0;
            
            
            while (true) {
                this.progressBar1.Step = 10;
            }
        }
                
        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TxtUserName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.txtUserPwd.Focus();
            }
        }

        private void TxtUserPwd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnLogin.Focus();
            }
        }

        private void F_Login_Load(object sender, EventArgs e)
        {
            this.progressBar1.Visible = false;
            string user_no = "", user_pwd = "";
            
            user_no = ini.GetINIFileValue("TempData", "user_no");
            user_pwd = ini.GetINIFileValue("TempData", "user_pwd");
            this.txtUserName.Text = user_no;
            this.txtUserPwd.Text = BaseClass.ChangeStr.GetNormarActionOut(user_pwd);
            if (this.txtUserName.Text.Trim() != "" && this.txtUserPwd.Text.Trim() != "") {
                btnLogin.Focus();
            }

            //SetMyStyle();
        }

        private void SetMyStyle()
        {
            this.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.BackGroudColor);
            foreach (Control con in this.Controls)
            {
                if (con is TextBox)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TextBoxColor);
                }
                else if (con is ToolStrip)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ToolStripColor);
                }
                else if (con is TreeView)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TreeViewColor);
                }
                else if (con is Button)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ButtonColor);
                }
                else if (con is ComboBox)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ComboBoxColor);
                }
                else if (con is Label)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.LabelColor);
                }
                else if (con.HasChildren)
                {
                    con.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.BackGroudColor);
                    foreach (Control conns in con.Controls)
                    {
                        if (conns is TextBox)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TextBoxColor);
                        }
                        else if (conns is ToolStrip)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ToolStripColor);
                        }
                        else if (conns is TreeView)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.TreeViewColor);
                        }
                        else if (conns is Button)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ButtonColor);
                        }
                        else if (conns is ComboBox)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.ComboBoxColor);
                        }
                        else if (conns is Label)
                        {
                            conns.BackColor = Color.FromArgb(255, BaseClass.BaseMethod.LabelColor);
                        }
                    }
                }
            }
        }
    }
}
