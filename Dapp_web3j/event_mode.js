var Myweb= require('web3');
var async = require("async");

const Tx = require('ethereumjs-tx');

var myweb = new Myweb();
const contract_address = "0x4B5E7D7CCdA15BEe398d952B6A169d38B2490a78";
const contract_event_api = "https://api-rinkeby.etherscan.io/api?module=logs&action=getLogs&fromBlock=379224&toBlock=latest&address="+contract_address+"&sort=asc";
//日志解析API
const contract_input = [{"indexed":false,"name":"_address","type":"address"},{"indexed":false,"name":"_name","type":"string"},{"indexed":false,"name":"_bookdata","type":"string"}];

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
    } else {                
    web3 = new Web3(new Web3.providers.HttpProvider("http://192.168.3.12:8545"));
    //http://192.168.3.12:8545
    //https://rinkeby.infura.io/v25WvmO8AvudaPfBMRMW 
    //https://rinkeby.infura.io/v25WvmO8AvudaPfBMRMW 
} 

var abi;                    
abi=[{"constant":true,"inputs":[{"name":"addr","type":"address"},{"name":"id","type":"uint256"}],"name":"AddKeygetBookData","outputs":[{"name":"temp_id","type":"uint256"},{"name":"bn","type":"string"},{"name":"bd","type":"string"},{"name":"bty","type":"string"},{"name":"bti","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"AddKeyAndOutTimeDataGet","outputs":[{"name":"temp_id","type":"uint256"},{"name":"addr","type":"address"},{"name":"bn","type":"string"},{"name":"bd","type":"string"},{"name":"bty","type":"string"},{"name":"bti","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"userId","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"}],"name":"denyAccess","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"accessAllowed","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_address","type":"address"},{"name":"_name","type":"string"},{"name":"_bookdata","type":"string"}],"name":"setBook","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"},{"name":"_myname","type":"string"},{"name":"_mybook","type":"string"},{"name":"_ty","type":"string"},{"name":"_outtime","type":"uint256"}],"name":"AddKeysetBook","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"}],"name":"allowAccess","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_address","type":"address"},{"indexed":false,"name":"_name","type":"string"},{"indexed":false,"name":"_bookdata","type":"string"}],"name":"Instructor","type":"event"}];
var infoContract = web3.eth.contract(abi);     
var info = infoContract.at(contract_address); 
//var MyContract = new web3.eth.contract(abi, contractAddress);
var temp_log;   //用来输出日志

var instructorEvent = info.Instructor();   

instructorEvent.watch(function(error, result) {
if (!error)
    {
        alert('有人发布成功');
        console.log(result);
        if (web3.isAddress($('#from_address').val())) {
            getDataAll($('#from_address').val(), '');
        } else { 
            getDataAll('','');
        }
    } else {
        //console.log(error);
    }
});

            
var isok_update=true;
var step='0 / 0';
web3.eth.isSyncing(function(error, sync){
if(!error) {
    // stop all app activity
    if(sync === true) {
    // we use `true`, so it stops all filters, but not the web3.eth.syncing polling
        web3.reset(true);                
    // show sync info
    } else if(sync) {
        console.log(sync.currentBlock);                
        step = sync.currentBlock + ' / ' + sync.highestBlock;   
        if (sync.currentBlock == sync.highestBlock) { 
            sync = true;
        }
        // re-gain app operation
    } else {
        // run your app init function...                    
        isok_update=false;
    }
}
});


var select =async function () { 
    //获取开通权限人数
    await info.userId(function (error, result) {
        if (!error) {
            if (result != null && result !== 'undefined' && result != '') { document.getElementById("private_book").value = (result); }
        }
        else
            console.error(error);
        });   
}

//每个5分钟获取一次数据
$(document).ready(function(){ 
    select();			
    setInterval(function(){
        select();
    }, 5*60*1000);	
})

//获取Event事件信息,并解析
$('#select_event').click(async function () {    
    getDataAll('','');    
});

getDataAll = function (t_address,t_hash) {     
    
    async.waterfall([
        function (callback) { 
            console.log('第1步：获取所有json串');
            var jsonObj;	//获取的数据
            $.getJSON(contract_event_api, function (data) {	        
                var JsonData;                  
                //var string = JSON.stringify(json)
                //JSON.parse(string)将字符串转为JSON格式                        
                JsonData = JSON.stringify(data);    //转换为Json字符串                
                if (JsonData != null) {
                    var temp_jsonObj
                    temp_jsonObj=JSON.parse(JsonData);   //将Json字符串转换为json对象
                    //console.log('************************************');        
                    //console.log(temp_jsonObj.status);
                    //console.log(temp_jsonObj.message);
                    //console.log(temp_jsonObj.result);
                    //console.log('************************************');       
                    if (temp_jsonObj.status == 1) {
                        //console.log('OK..........');
                        jsonObj = temp_jsonObj.result;
                        callback(null, jsonObj);   
                    } else { 
                        //console.log('NO..........');                                     
                        callback(new Error("获取信息失败！"));
                    }            
                } else {                         
                    //console.log('error..........');
                    callback(new Error("获取信息出错！"));
                }        
            });
            //callback(null,b)在这里通过回调函数把b传给下一个函数，
            //记得一定要加上null，才能调用数组中得下一个函数，
            //否则，会直接调用最终的回调函数，然后结束函数，则后面的函数将不再执行
        },
        function (jsonObj, callback) { 
            //console.log('第2步,解析json对象并组合为table' );    
            //console.log(JSON.stringify(jsonObj, null, ' '));  
            $('#jsb_amount').val(jsonObj.length+'篇');

            var trStr = '';//动态拼接table 
            if (t_address != 'undefined' && t_address != '') {
                //console.log('指定账户查询' + t_address);
                for (var i = 0; i < jsonObj.length; i++) {
                    //console.log('第' + i + '次取值****************************************************');
                    var res = myweb.eth.abi.decodeLog(contract_input, jsonObj[i].data, jsonObj[i].topics);
                    if (String(res[0]).toUpperCase() == t_address.toUpperCase()) {
                        trStr = trStr + "发布时间 " + getLocalTime(jsonObj[i].timeStamp) + "&nbsp;&nbsp;      区块数" + scientificToNumber(jsonObj[i].blockNumber, 16) + "&nbsp;&nbsp;     花费gas=" + scientificToNumber(jsonObj[i].gasUsed, 16);
                        trStr = trStr + "<br>";
                        trStr = trStr + "发表人账户：" + res[0];
                        trStr = trStr + "<br>";
                        trStr = trStr + "查询哈希：" + jsonObj[i].transactionHash;
                        trStr = trStr + "标题:" + res[1];
                        trStr = trStr + "<br>";
                        trStr = trStr + "内容:" + res[2];
                        trStr = trStr + "<br>";
                        trStr = trStr + "<br>";
                    }
                    else { 
                        //console.log(res[0]+'='+t_address);
                    }                    
                }
            } else if (t_hash != 'undefined' && t_hash != '') {
                //console.log('指定哈希查询' + t_hash);
                for (var i = 0; i < jsonObj.length; i++) {
                    //console.log('第' + i + '次取值****************************************************');
                    if (String(jsonObj[i].transactionHash).toUpperCase() == t_hash.toUpperCase()) { 
                        var res = myweb.eth.abi.decodeLog(contract_input, jsonObj[i].data, jsonObj[i].topics);
                        trStr = trStr + "发布时间 " + getLocalTime(jsonObj[i].timeStamp) + "&nbsp;&nbsp;      区块数" + scientificToNumber(jsonObj[i].blockNumber, 16) + "&nbsp;&nbsp;     花费gas=" + scientificToNumber(jsonObj[i].gasUsed, 16);
                        trStr = trStr + "<br>";
                        trStr = trStr + "发表人账户：" + res[0];
                        trStr = trStr + "<br>";
                        trStr = trStr + "查询哈希：" + jsonObj[i].transactionHash;
                        trStr = trStr + "标题:" + res[1];
                        trStr = trStr + "<br>";
                        trStr = trStr + "内容:" + res[2];
                        trStr = trStr + "<br>";
                        trStr = trStr + "<br>";    
                    }
                    //else { 
                    //    console.log(res[0]+'='+t_hash);
                    //}                 
                }
            } else {
                for (var i = 0; i < jsonObj.length; i++) {
                    //console.log('第' + i + '次取值****************************************************');
                    //console.log('topics= '+jsonObj[0].topics[0]);
                    //console.log('data= '+jsonObj[0].data);
                    //console.log('gasused= '+jsonObj[0].gasUsed);
                    //console.log('trhash= ' + jsonObj[0].transactionHash);
                    //console.log('第' + i + '次解析****************************************************');
                    var res = myweb.eth.abi.decodeLog(contract_input, jsonObj[i].data, jsonObj[i].topics);
                    //console.log(res[0]);
                    //console.log(res[1]);
                    //console.log(res[2]);
                    //console.log('第' + i + '次组合****************************************************');
                    trStr = trStr + "发布时间 " + getLocalTime(jsonObj[i].timeStamp) + "&nbsp;&nbsp;      区块数" + scientificToNumber(jsonObj[i].blockNumber, 16) + "&nbsp;&nbsp;     花费gas=" + scientificToNumber(jsonObj[i].gasUsed, 16);
                    trStr = trStr + "<br>";
                    trStr = trStr + "发表人账户：" + res[0];
                    trStr = trStr + "<br>";
                    trStr = trStr + "查询哈希：" + jsonObj[i].transactionHash;
                    trStr = trStr + "标题:" + res[1];
                    trStr = trStr + "<br>";
                    trStr = trStr + "内容:" + res[2];
                    trStr = trStr + "<br>";
                    trStr = trStr + "<br>";
                }
            }
            callback(null,trStr);            
        }
    ]
    ,function (err, result) { 
            //console.log('第三步将table放入对应位置****************');    
            if (err) {
                //console.log(err);
            } else { 
                //console.log(result);                
                if(result!=null){
                    document.getElementById("chartHours").innerHTML=result;
                } else {
                    document.getElementById("chartHours").innerHTML='';
                }
                
            }
        }

    );
}

//指定账户查询 
$('#select_data').click(function(){ 
    if (web3.isAddress($('#own_address').val())) {
        getDataAll($('#own_address').val(), '');
    } else { 
        alert('输入地址不合法');
    }
});

//指定hash查询
$('#select_hash').click(function () { 
    if ($('#own_hash').val()!='undefined' && $('#own_hash').val()!='' ) {
        getDataAll('',$('#own_hash').val());
    } else { 
        alert('输入哈希不合法');
    }
});

//按照账户查询锁定信息
var temp_data; //存储锁定信息
var flag = 'A';
$('#select_data_check').click(function () {    
    temp_data = '';    
    flag = 'A';
    if (!web3.isAddress($('#own_address').val())) {                    
        alert("输入地址不合法");
        return;
    }
    var _from = $('#own_address').val();                            
    getKeyDataC(_from,0);        
});

function getKeyDataC(_from,_int) {     
    async.waterfall([        
        function (callback) {                        
            if (flag == 'A') {
                _int=0;
                flag = 'B';
            }
            info.AddKeygetBookData(_from, _int,function (err, result) {
                if (err) {
                    temp_data = err;
                    _int = 0;   
                    callback(new Error("错误"+err));
                } else {
                    //console.log('*****第'+_int+'次***********');
                    //console.log(result);
                    //console.log('****************');
                    _int = result[0].c[0];
                    //console.log('内i=' + _int);
                    if (_int > 0) {
                        temp_data = temp_data + '名称：' + result[1] + '<br>';
                        if (result[3] == 'txt') {                            
                            temp_data = temp_data + '内容：' + result[2] + '<br>';
                            temp_data = temp_data + '类型：' + result[3] + '<br>';
                            console.log('时间'+result[4].c[0]);
                            temp_data = temp_data + '时间：' + formatDateTime(result[4].c[0]) + '<br>';
                            
                        } else {
                            temp_data = temp_data + '<input type="image" src="' + result[2] + '" />';   
                        }
                        temp_data = temp_data + '<br>';    
                        //console.log('i类型='+typeof(i));
                        //console.log('aaaa内i=' + _int);
                        //console.log(temp_data);
                        getKeyDataC(_from,_int);
                    }             
                    if (_int == 0) {
                        callback(null,temp_data);    
                    }            
                }
            });
            
        }
        ]
        ,function (err, result) { 
            if (err) {
                console.log(err);
            } else {                 
                if (temp_data == '') {
                    temp_data = '没有数据可以查！';
                }                 
                document.getElementById("chartHours").innerHTML=result;
            }
        }
    );
}
//查询所有解锁信息
$('#select_event_check').click(function () {
    flag = 'A';
    temp_data = '';
    getKeyDataOutTime(0);
});

function getKeyDataOutTime(_int) { 
    async.waterfall([        
        function (callback) {            
            if (flag == 'A') {
                _int = parseInt($('#private_book').val())+1;
                if (_int == 'undefined' ||  _int=='') {
                    _int=0;                
                }    
                flag = 'B';
            }
            //console.log('_int'+_int);

            info.AddKeyAndOutTimeDataGet(_int,function (err, result) {
                if (err) {
                    temp_data = err;
                    _int = 0;   
                    callback(new Error("错误"+err));
                } else {
                    //console.log(result[0].c[0]);
                    //console.log('*****第'+_int+'次***********');
                    //console.log(result);
                    //console.log('****************');
                    _int = result[0].c[0];
                    if (_int > 0 && result[2]!='') {
                        //temp_data = temp_data + '地址：' + result[1] + '<br>';
                        //temp_data = temp_data + '名称：' + result[2] + '<br>';
                        //temp_data = temp_data + '内容：' + result[3] + '<br>';
                        //temp_data = temp_data + '类型：' + result[4] + '<br>';
                        //temp_data = temp_data + '时间：' + formatDateTime(result[5].c[0]) + '<br>';
                        //temp_data = temp_data + '<br>';

                        temp_data = temp_data + '地址：' + result[1] + '<br>';
                        temp_data = temp_data + '名称：' + result[2] + '<br>';
                        if (result[4] == 'txt') {                            
                            temp_data = temp_data + '内容：' + result[3] + '<br>';
                            temp_data = temp_data + '类型：' + result[4] + '<br>';
                            temp_data = temp_data + '时间：' + formatDateTime(result[5].c[0]) + '<br>';
                            
                        } else {
                            temp_data = temp_data + '<input type="image" src="' + result[3] + '" />';   
                        }
                        temp_data = temp_data + '<br>';   

                        getKeyDataOutTime(_int);
                    }             
                    if (_int == 0) {
                        callback(null,temp_data);    
                    }            
                }
            });            
        }
        ]
        ,function (err, result) { 
            if (err) {
                console.log(err);
            } else {                 
                if (temp_data == '') {
                    temp_data = '没有数据可以查！';
                }                 
                document.getElementById("chartHours").innerHTML=result;
            }
        }
    );
}

//结果校验
$('#check_hash').click(async function(){
    if($('#own_hash').val()!='undefined' && $('#own_hash').val()!=''){				
        web3.eth.getTransactionReceipt($('#own_hash').val(),async function(err, result) {
            if (!err) {     
                //console.log('******************')
                //console.log(result)
                //console.log('******************')
                //console.log("返回结果" + JSON.stringify(result, null, ' '));
                //document.getElementById("log_data").innerHTML=JSON.stringify(result, null, ' ');
                if (result == null) { 
                    alert('还没有交易打包,请等几分钟再次查询！');
                }
                else if(JSON.stringify(result, null, ' ') == null) { 
                    alert('还没有交易打包,请等几分钟再次查询！');
                } else {
                    //alert(result.status);
                    var res=result.status;
                    res=res.slice(2);
                    if (res == '1') {
                        res = '发布成功'
                    } else if (res == '0') {
                        res = '发布失败';
                    } else { 
                        res = '未知 '+res;
                    }
                    alert(res);                    
                    document.getElementById("chartHours").innerHTML=res;
                }
                
                
            } else {
                //console.log('=====================')
                //console.log(err)
                //console.log('=====================')
                
                alert(err);
            }
        });						
    }else{
        alert('请先输入交易结果返回哈希码！');
    }			
});

//时间戳转数字
function getLocalTime(temp_time) {      
    return new Date(parseInt(temp_time) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ');  
}

//科学计数、16进制数字 转普通数字
function scientificToNumber(num,step) {			
    //console.log('===='+parseInt(jsonObj.result[0].data.slice(2),16));
    if(step==null && step==''){
        step=16;
    }
    var str='';
    num=String(num);
    if(num.substring(0,2)=='0x'){
        str=num.slice(2);
        str = String(parseInt(str,step));			
    }else{
        str=num;
    }						
    
    //console.log('开始解析'+str);
    
    var left,right,result,temp;
    result='';
    if(str.indexOf('e+')>0){
        
        left=str.substring(0,str.indexOf('e+'));
        //console.log('左边'+left);
        right=str.substring(str.indexOf('e+')+2);
        //console.log('右边'+right);
        if(parseInt(right,10)>18){
            temp=parseInt(right,10)-18;	//合约位数设置为18
        }else{
            temp=parseInt(right,10);
        }
        result=left+'e+'+temp;				
        //result=parseFloat('1'+result);
        //console.log('左边转换结果='+parseFloat(left)+'  右边转换结果'+result);				
        result=new Decimal(result);				
        result=result.toFixed();

    }else{
        var point=18;	//小数位
        var lost=1;	//记录去除多少个0				
        result=str;	//复制一份
        if(str.length>point){
            //console.log('长数字'+result)				
            for(var i=str.length;i>=0;i--){
                temp=str.substring(i,1);
                if(lost<=18){
                    if(parseInt(temp,10)==0){
                        result=result.substring(0,result.length-1);
                    }else{
                        //当去除第10个不是零时，要加小数位上去
                        left=result.substring(0,result.length-(point-lost));
                        right=result.substring(result.length-(point-lost));
                        result=left+'.'+right;
                        var num = new Number(result);
                        result=num;
                        break;
                    }
                    lost++;
                }else{
                    break;	//最多去除18个零
                }
            }
        }else{
            //console.log('小于18位计数='+str)
            for(var i=str.length;i>=0;i--){
                temp=str.substring(i,1);						
                if(parseInt(temp,10)==0){
                    result=result.substring(0,result.length-1);
                }else{
                    //去掉多余的0
                    break;
                }
            }
            var num = new Number(result);
            result=num;
        }
    }
    return result;
}

//时间戳转标准格式
function formatDateTime(inputTime) {
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}

//发布
$("#submit_file").click(async function () {
    if (isok_update) {
        temp_log = '';
        async.waterfall([
            function (callback) {  
                if (!web3.isAddress($('#from_address').val())) {                    
                    callback(new Error("输入地址不合法"));
                }
                var _from = $('#from_address').val();                        
                console.log('权限测试');
                
                info.accessAllowed(_from, function (error, result) {                    
                    if (!error) {
                        //$("#db_name").html(result[0]);
                        //alert((result));                
                        temp_log = temp_log + '发布权限：' + result + '\n\t';
                        $('#log_data').val(temp_log);    //后台日志输出
                        
                        console.log('db_name=' + result); 
                        if (result) {
                            callback(null, _from);
                        } else { 
                            callback(new Error("输入地址"+_from+"没有发布权限,请联系管理员开通权限!"));
                        }                        
                    }
                    else {
                        console.error(error);                
                        callback(new Error("输入地址"+_from+"发布权限获取出错,请重新尝试!"));
                    }
                        
                });                        
                //info.accessAllowed(_from);
            },
            function (_from, callback) {
                console.log('签名发起人'+_from);
                console.log('**************************************');
                $('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块
                con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
                if (con == true) {   
                    if ($('#private_key').val() == 'undefined' || $('#private_key').val() == '') {
                        callback(new Error("请输入私钥！"));                        
                    }
                    var privatekey = $('#private_key').val();

                    if ($('#from_name').val() == 'undefined' || $('#from_name').val() == '') {
                        callback(new Error("请输入合适的文章名称！"));                        
                    }
                    var _bookname = $('#from_name').val();

                    if (document.getElementById("chartHours").innerHTML == '') {
                        callback(new Error("请输入合适的文章内容！"));                        
                    }                    
                    var _bookdata = $.trim(document.getElementById("chartHours").innerHTML);
                    //$('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块

                    var data = info.setBook.getData(_from, _bookname, _bookdata);
                    
                    temp_log = temp_log + '************生成信息*************** \n';
                    temp_log = temp_log + data +'\n';
                    $('#log_data').val(temp_log);    //后台日志输出

                    var gas = $('#from_gas').val();            
                    if (gas == '') {
                        alert('Gas is too low!');
                        callback(new Error("Gas太低,请适当调高！"));    
                    }
                    
                    _to = contract_address;
                    
                    submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
                    callback(null, true);
                }
            }
        ],
        function (err, result) { 
            console.log('发布结果****************');    
                if (err) {
                    //console.log(err);
                    temp_log = temp_log + err+'\n';
                } else {
                    //console.log(result);
                    temp_log = temp_log + result+'\n';
                }            
            
            $('#log_data').val(temp_log);    //后台日志输出
            }
        );

    } else {
        alert('正在同步数据，请等一会再查询！同步进度= ' + step);
    }
});

//开通
$("#submit_open").click(async function () {    
    if (isok_update) {
        temp_log = '';
        console.log('签名交易开始');
        console.log('**************************************');
        con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
        if (con == true) {
            if (!web3.isAddress($('#adm_address').val())) {
                alert('输入地址不合法!');
                return;
            }
            var _from = $('#adm_address').val();

            if (!web3.isAddress($('#open_address').val())) {
                alert('输入地址不合法!');
                return;
            }
            var _open = $('#open_address').val();

            if ($('#adm_key').val() == 'undefined' || $('#adm_key').val() == '') {
                alert('请输入私钥！');
                return;
            }
            var privatekey = $('#adm_key').val();
            
            $('#adm_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块            
            var data = info.allowAccess.getData(_open);

            var gas = $('#adm_gas').val();            
            if (gas == '') {
                alert('Gas is too low!');
                return;
            }
            
            _to = contract_address;
            
            submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
        }
    } else {
        alert('正在同步数据，请等一会再查询！同步进度= ' + step);
    }
});

//禁止
$("#submit_close").click(async function () {
    if (isok_update) {
        console.log('签名交易开始');
        console.log('**************************************');
        con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
        if (con == true) {
            if (!web3.isAddress($('#adm_address').val())) {
                alert('输入地址不合法!');
                return;
            }
            var _from = $('#adm_address').val();

            if (!web3.isAddress($('#close_address').val())) {
                alert('输入地址不合法!');
                return;
            }
            var _close = $('#close_address').val();

            if ($('#adm_key').val() == 'undefined' || $('#adm_key').val() == '') {
                alert('请输入私钥！');
                return;
            }
            var privatekey = $('#adm_key').val();
            
            $('#adm_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块            
            var data = info.denyAccess.getData(_close);

            var gas = $('#adm_gas').val();            
            if (gas == '') {
                alert('Gas is too low!');
                return;
            }
            
            _to = contract_address;
            
            submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
        }
    } else {
        alert('正在同步数据，请等一会再查询！同步进度= ' + step);
    }
});

//转移
$("#submit_power").click(async function () {
    if (isok_update) {
        console.log('签名交易开始');
        console.log('**************************************');
        //增加权限校验 通过才能进行下一步
        if (!web3.isAddress($('#adm_address').val())) {
            alert('输入地址不合法!');
            return;
        }
        var _from = $('#adm_address').val();

      
        
            con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
            if (con == true) {
            

                if (!web3.isAddress($('#power_address').val())) {
                    alert('输入地址不合法!');
                    return;
                }
                var _power = $('#power_address').val();

                if ($('#adm_key').val() == 'undefined' || $('#adm_key').val() == '') {
                    alert('请输入私钥！');
                    return;
                }
                var privatekey = $('#adm_key').val();
            
                $('#adm_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块            
                var data = info.denyAccess.getData(_power);

                var gas = $('#adm_gas').val();
                if (gas == '') {
                    alert('Gas is too low!');
                    return;
                }
            
                _to = contract_address;
            
                submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
            }
        
    } else {
        alert('正在同步数据，请等一会再查询！同步进度= ' + step);
    }
});

//锁定时间发布
$('#submit_file_time').click(async function () { 
    async.waterfall([        
        function (callback) { 
            if (isok_update) {
                var reg=new RegExp("^[0-9]*$");
                var add_time = $('#add_time').val(); 
                console.log(add_time);            
                if (reg.test(add_time)) {
                    var t = new Date().getTime();
                    console.log(t);        
                    add_time = new Date(t+(add_time*60*1000)).getTime();
                    console.log(add_time);
                    
                    callback(null, add_time);
                } else { 
                    callback(new Error("请填写合适的时间,单位分钟 "));  
                }    
            } else {
                callback(new Error("正在同步数据，请等一会再查询！同步进度= " + step));  
            }            
        }
        ,function (add_time,callback) {  
            if (!web3.isAddress($('#from_address').val())) {                    
                callback(new Error("输入地址不合法"));
            }
            var _from = $('#from_address').val();                        
            
            info.accessAllowed(_from, function (error, result) {                    
                if (!error) {
                    //$("#db_name").html(result[0]);
                    //alert((result));                
                    $('#log_data').val('发布权限：'+result);    //后台日志输出
                    console.log('db_name=' + result); 
                    if (result) {
                        callback(null, _from,add_time);
                    } else { 
                        callback(new Error("输入地址"+_from+"没有发布权限,请联系管理员开通权限!"));
                    }                        
                }
                else {
                    console.error(error);                
                    callback(new Error("输入地址"+_from+"发布权限获取出错,请重新尝试!"));
                }
                    
            });                        
            //info.accessAllowed(_from);
        },
        function (_from,add_time, callback) {
            console.log('签名发起人'+_from+' 锁定时间'+add_time);
            console.log('**************************************');
            $('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块
            con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
            if (con == true) {   
                if ($('#private_key').val() == 'undefined' || $('#private_key').val() == '') {
                    callback(new Error("请输入私钥！"));                        
                }
                var privatekey = $('#private_key').val();

                if ($('#from_name').val() == 'undefined' || $('#from_name').val() == '') {
                    callback(new Error("请输入合适的文章名称！"));                        
                }
                var _bookname = $('#from_name').val();

                if (document.getElementById("chartHours").innerHTML == '') {
                    callback(new Error("请输入合适的文章内容！"));                        
                }                    
                var _bookdata = $.trim(document.getElementById("chartHours").innerHTML);
                //$('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块
                
                var ty = 'txt';

                var data = info.AddKeysetBook.getData(_from, _bookname, _bookdata,ty,add_time);
                
                temp_log = temp_log + '************生成信息***************'+'\n';
                temp_log = temp_log + data+'\n';
                $('#log_data').val(temp_log);    //后台日志输出

                var gas = $('#from_gas').val();            
                if (gas == '') {
                    alert('Gas is too low!');
                    callback(new Error("Gas太低,请适当调高！"));    
                }
                
                _to = contract_address;
                
                submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
                callback(null, true);
            }
        }
    ],
        function (err, result) { 
            console.log('发布结果****************');    
            if (err) {
                //console.log(err);
                temp_log = temp_log + err+'\n';
            } else { 
                //console.log(result);                        
                temp_log = temp_log + result+'\n';
            }
            $('#log_data').val(temp_log);    //后台日志输出
        }        
    );
})

//发布图片
$('#submit_file_img').click(function () {
    async.waterfall([function (callback) { 
        if (isok_update) {
            var reg=new RegExp("^[0-9]*$");
            var add_time = $('#add_time').val(); 
            console.log(add_time);            
            if (reg.test(add_time)) {
                var t = new Date().getTime();
                console.log(t);        
                add_time = new Date(t+(add_time*60*1000)).getTime();
                console.log(add_time);
                
                callback(null, add_time);
            } else { 
                callback(new Error("请填写合适的时间,单位分钟 "));  
            }    
        } else {
            callback(new Error("正在同步数据，请等一会再查询！同步进度= " + step));  
        }            
    }
    ,function (add_time,callback) {  
        if (!web3.isAddress($('#from_address').val())) {                    
            callback(new Error("输入地址不合法"));
        }
        var _from = $('#from_address').val();                        
        
        info.accessAllowed(_from, function (error, result) {                    
            if (!error) {
                //$("#db_name").html(result[0]);
                //alert((result));                
                $('#log_data').val('发布权限：'+result);    //后台日志输出
                console.log('db_name=' + result); 
                if (result) {
                    callback(null, _from,add_time);
                } else { 
                    callback(new Error("输入地址"+_from+"没有发布权限,请联系管理员开通权限!"));
                }                        
            }
            else {
                console.error(error);                
                callback(new Error("输入地址"+_from+"发布权限获取出错,请重新尝试!"));
            }
                
        });                        
        //info.accessAllowed(_from);
    },
    function (_from,add_time, callback) {
        console.log('签名发起人'+_from+' 锁定时间'+add_time+'图片');
        console.log('**************************************');
        $('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块
        con = confirm("你确定要发表文章！"); //在页面上弹出对话框    
        if (con == true) {   
            if ($('#private_key').val() == 'undefined' || $('#private_key').val() == '') {
                callback(new Error("请输入私钥！"));                        
            }
            var privatekey = $('#private_key').val();

            if ($('#from_name').val() == 'undefined' || $('#from_name').val() == '') {
                callback(new Error("请输入合适的文章名称！"));                        
            }
            var _bookname = $('#from_name').val();

            if ($('#tmpData').val() == 'undefined' || $('#tmpData').val() == '') {
                callback(new Error("请选择图片！"));                        
            }         

            var _bookdata = $('#tmpData').val(); // $.trim(document.getElementById("chartHours").innerHTML);
            //$('#from_gas').val(web3.eth.gasPrice.toString(10)); //高出价，速度出块
            
            var ty = 'jpg';

            var data = info.AddKeysetBook.getData(_from, _bookname, _bookdata,ty,add_time);
            
            temp_log = temp_log + '************生成信息***************'+'\n';
            temp_log = temp_log + data+'\n';
            $('#log_data').val(temp_log);    //后台日志输出

            var gas = $('#from_gas').val();            
            if (gas == '') {
                alert('Gas is too low!');
                callback(new Error("Gas太低,请适当调高！"));    
            }
            
            _to = contract_address;
            
            submit_data(_from, _to, 0, data, gas, privatekey);    //这里是发送代币,所以 0            
            callback(null, true);
        }
    }]);
});

submit_data = function (_from,_to,_value,_data,_gas,_privatekey) {     
    async.waterfall([
        function (callback) { 
            //console.log('第一步 基础数据校验');
            //console.log('传入参数  _from ' +_from); 
            //console.log('传入参数  _to ' +_to); 
            //console.log('传入参数  _value ' +_value); 
            //console.log('传入参数  _data ' + _data); 
            //console.log('传入参数  _gas ' +_gas); 
            //console.log('传入参数  _privatekey ' +_privatekey);            
            
            const mgasPrice =web3.toHex(_gas); 
            const mgasLimit=web3.toHex(7000000);        
            console.log('gasLimit='+mgasLimit+'  gasPrice='+mgasPrice+' '+web3.eth.gasPrice);
            
            var txRow = {
                nonce: web3.toHex((web3.eth.getTransactionCount(_from,"pending"))),//随机数 pending 包括
                //gasPrice和gasLimit如果不知道怎么填，可以参考etherscan上的任意一笔交易的值
                from:_from,
                gasPrice: mgasPrice,   //0x77359400
                gasLimit: mgasLimit,      //'0x295f05'
                to: _to,//接受方地址或者合约地址 发送代币是填合约地址、发送以太币填接收地址
                value: '0x00',//发送的金额，这里是16进制，实际表示发送256个wei
                data: _data,
                chainId: 4
            }

            const ok_privateKey = Buffer.from(_privatekey, 'hex');
            var tx = new Tx(txRow);
            console.log(JSON.stringify(txRow, null, ' '));
            temp_log = temp_log + '**********签名信息**************'+'\n';
            temp_log = temp_log + JSON.stringify(txRow, null, ' ') + '\n';            
            $('#log_data').val(temp_log);    //后台日志输出

            tx.sign(ok_privateKey);        
            var serializedTx = tx.serialize();    
            if (tx.verifySignature()) {
                //console.log('Signature Checks out!')
            }            
            //console.log(serializedTx.toString('hex'));
            //result = serializedTx.toString('hex');    
            
            temp_log = temp_log + '**********发送信息**************'+'\n';
            temp_log = temp_log + '0x' + serializedTx.toString('hex') + '\n';            
            $('#log_data').val(temp_log);    //后台日志输出

            web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'),async function(err, hash) {
                if (!err){                    
                    //console.log("结果校验" + web3.eth.getTransactionReceipt(hash));                    
                    callback(null,hash);
                }else{                        
                    callback(new Error('错误'+err));                    
                }
            });        
        }
    ], function (err, result) { 
        console.log('第二步获取发布结果****************');    
        if (err) {            
            var temp_err = '' + err;
            console.log(temp_err);
            if (temp_err == 'Error: 错误Error: replacement transaction underpriced') {
                temp_err = '您有一笔交易正在等待打包,请等1-3分钟再次发起交易！';
            }
            temp_log = temp_log + err + '\n';            
            alert(temp_err);
        } else { 
            //console.log(result);
            document.getElementById("chartHours").innerHTML = result;
            alert('发布完成,请等待验证！');
            $('#own_hash').val(result);
            temp_log = temp_log + result + '\n';            
        }        
        $('#log_data').val(temp_log);    //后台日志输出
    });
}


